# code_download

## 一、脚本介绍

### 1.1 概述

本仓脚本用于项目中下载`ohos`分支代码使用，省去下载代码时找对应分支的命令麻烦

### 1.2 路径配置

因每个人当第一次使用该脚本下载代码的时候会提示设置代码下载的路径，然后会将该路径保存在本地的`.config`文件中，后面再次下载则不再提示

- 不输入直接回车，则默认保存在`~/OpenHarmony`里面
- 输入设置`.`，则表示保存在脚本所在的目录下面
- 其他路径可自行设置，但必须为**绝对路径**

运行效果如下：

```shell
first download code, please set code download path, default is ~/OpenHarmony
. # 这里输入想存放的路径
```

### 1.3 分支介绍

脚本运行后如下所示，后续添加其他版本可能有增加，以实际效果为准

```shell
********************************************
*   Welcome to download OpenHarmony Code   *
* Please Choice OpenHarmony SDK:           *
* OpenHarmony Version                      *
*   OpenHarmony master         prese  1.1  *
*   OpenHarmony-v3.2.2-Release press  1.2  *
*   OpenHarmony-v4.0-Release   press  1.3  *
*   OpenHarmony-v4.1-Release   press  1.4  *
*   OpenHarmony-clang-master   press  1.x  *
* Thead Products Version                   *
*   dayu800-v3.2.2-release     press  2.1  *
*   dayu800-v4.0-release       press  2.2  *
*   dayu800-v4.1-release       press  2.3  *
*   dayu800-5.0-release        press  2.4  *
*   thead-tablet               press  2.20 *
*   thead-cartablet            press  2.21 *
*   thead-cloud                press  2.22 *
*   dayu800-lt9211-v4.1        press  2.23 *
*   dayu800-cloud-gateway-v4.1 press  2.24 *
*   dayu800-v4.1-open-source   press  2.25 *
*   kernel-xts-master          press  2.26 *
*   future-city-v3.2.2         press  2.27 *
*   future-city-v4.1           press  2.28 *
*   ienvctrl-v4.1              press  2.29 *
*   duolun-v4.1-release        press  2.30 *
*   dayu810-v4.0-release       press  2.31 *
*   dayu800-cloud-zhihe        press  2.32 *
*   dayu800-zhihe-air          press  2.33 *
*   dayu800-zhihe-duolun       press  2.34 *
*   dayu800-thead-pad          press  2.35 *
*   chromium-v4.0-release      press  2.51 *
*   chromium-v4.1-release      press  2.52 *
* Hisilicon Products Version               *
*   dayu900-v4.0-small         press  3.11 *
*   dayu900-v4.0-ebaina-camera press  3.14 *
*   dayu900-v4.0-sdk           press  3.15 *
*   ebaina-hi351xdv500         press  3.50 *
*   ebaina-hi351xdv500-ohos    press  3.51 *
*   power-v4.0-ebaina          press  3.52 *
*   power-v4.0-dayu900         press  3.53 *
*   power-v4.0-dayu900-open    press  3.54 *
*   power-v4.0-dayu900_sec     press  3.55 *
*   power-v4.0-release-tag     press  3.x  *
* Space China Version                      *
*   dayu210-v4.1-release       press  4.1  *
*   speed-v4.1-release         press  4.2  *
*   hihope-v4.1-release        press  4.3  *
*   njdg-v4.1-release          press  4.4  *
********************************************
```

- **`OpenHarmony Version`表示下载的`ohos`官方的对应分支**
  - `OpenHarmony master`：表示`OpenHarmony`官方`master`分支源代码
  - `OpenHarmony-v3.2.2-Release`：表示`OpenHarmony`官方`OpenHarmony-v3.2.2-Release`的tag源代码
  - `OpenHarmony-v4.0-Release`：表示`OpenHarmony`官方`OpenHarmony-v4.0-Release`的tag源代码
  - `OpenHarmony-v4.1-Release`：表示`OpenHarmony`官方`OpenHarmony-v4.1-Release`的tag源代码
  - `OpenHarmony clang-master`：表示`OpenHarmony`官方`clang`工具链`master`分支源代码
- **`Thead Products Version`表示下载的`thead`项目的开发分支**

  - `dayu800-v3.2.2-release`：表示基于`OpenHarmony`的`OpenHarmony-v3.2.2-Release`的tag适配的dayu800代码
  - `dayu800-v4.0-release`：表示基于`OpenHarmony`的`OpenHarmony-v4.0-Release`的tag适配的dayu800代码，对应soc是TH1520
  - `dayu800-v4.1-release`：表示基于`OpenHarmony`的`OpenHarmony-v4.1-Release`的tag适配的dayu800代码，对应soc是TH1520
  - `dayu800-5.0-release`：表示基于`OpenHarmony`的`dayu800-5.0-release`的tag适配的dayu800代码，对应soc是TH1520
  - `thead-tablet`：基于`dayu800`的`OpenHarmony-v3.2.2-Release`分支适配的th1520的平板项目代码
  - `thead-cartablet`：基于`dayu800`的`OpenHarmony-v3.2.2-Release`分支适配的th1520的车载平板项目代码
  - `thead-cloud`：基于`dayu800`的`OpenHarmony-v3.2.2-Release`分支适配的th1520的云桌面和边缘计算网关项目代码
  - `dayu800-lt9211-v4.1`：基于`dayu800`的`OpenHarmony-v4.1-Release`分支适配的lt9211屏幕代码
  - `dayu800-cloud-gateway-v4.1`：基于`dayu800`的`OpenHarmony-v4.1-Release`分支适配的th1520的云桌面和边缘计算网关项目代码
  - `dayu800-v4.0-open-source`：表示基于`OpenHarmony`的`OpenHarmony-4.0-Release`分支计划开源的代码
  - `kernel-xts-master`：表示基于`OpenHarmony`的`master`分支xts内核共建项目
  - `future-city-v3.2.2`：表示基于`OpenHarmony`的`OpenHarmony-v3.2.2-Release`分支适配的未来城市/明火识别代码
  - `future-city-v4.1`：表示基于`OpenHarmony`的`OpenHarmony-v4.1-Release`分支适配的未来城市/明火识别代码
  - `ienvctrl-v4.1`：表示基于`OpenHarmony`的`OpenHarmony-v4.1-Release`分支适配的智慧环控代码
  - `duolun-v4.1-release`:表示基于`OpenHarmony`的`OpenHarmony-v4.1-Release`分支适配的多伦代码
  - `dayu810-v4.0-release`:表示基于`OpenHarmony`的`OpenHarmony-v4.0-Release`分支适配的dayu810代码
  - `dayu800-cloud-zhihe`:表示基于`OpenHarmony`的`OpenHarmony-v4.1-Release`分支适配的知和的云桌面终端代码
  - `dayu800-zhihe-air`:表示基于`OpenHarmony`的`OpenHarmony-v4.1-Release`分支适配的知和空气质量检测仪代码
  - `dayu800-zhihe-duolun`:表示基于`OpenHarmony`的`OpenHarmony-v4.1-Release`分支适配的知和的多伦系统的平板代码
  - `dayu800-thead-pad`:表示基于`OpenHarmony`的`OpenHarmony-v4.1-Release`分支适配的多伦平板代码
  - `chromium-v4.0-release`：表示基于`OpenHarmony`的`OpenHarmony-v4.0-Release`分支适配的chromium代码
  - `chromium-v4.1-release`：表示基于`OpenHarmony`的`OpenHarmony-v4.1-Release`分支适配的chromium代码
- **`Hisilicon Products Version`**表示下载的海思项目的产品分支
  - `dayu900-v4.0-small`：表示使用`aarch64-v01c01-linux-musl-gcc`工具链基于OpenHarmony-v4.0-Release分支开发的hi3516dv500版本
  - `dayu900-v4.0-ebaina-camera`：表示使用`aarch64-v01c01-linux-musl-gcc`工具链基于OpenHarmony-v4.0-Release分支开发的易百纳hi3516dv500的web camera版本
  - `dayu900-v4.0-sdk`：表示使用`aarch64-v01c01-linux-musl-gcc`工具链基于OpenHarmony-v4.0-Release分支开发的易百纳hi3516dv500的sdk版本
  - `ebaina-hi351xdv500`：表示易百纳原生的代码
  - `ebaina-hi351xdv500-ohos`：表示基于易百纳原生代码编译鸿蒙使用的ko
  - `power-v4.0-ebaina`：表示适配易百纳hi351xdv500的电鸿v4.0版本
  - `power-v4.0-dayu900`：表示电鸿v4.0原生版本
  - `power-v4.0-dayu900-open`:表示适配的华威版本的电鸿v4.0版本
  - `power-v4.0-dayu900_sec`:表示用于过安全认证的电鸿v4.0版本
  - `power-v4.0-release-tag`：表示电鸿v4.0原生版本的tag

- **`Space China Version`**表示下载的智能院和典格项目的产品分支
  - `dayu210-v4.1-release`:表示基于`OpenHarmony`的`OpenHarmony-v4.1-Release`分支适配的dayu210代码
  - `speed-v4.1-release`:表示基于`OpenHarmony`的`OpenHarmony-v4.1-Release`分支适配的斯贝达项目代码
  - `hihope-v4.1-release`:表示基于`OpenHarmony`的`OpenHarmony-v4.1-Release`分支适配的智能院项目代码
  - `njdg-v4.1-release`:表示基于`OpenHarmony`的`OpenHarmony-v4.1-Release`分支适配的典格项目代码

![icon-note.gif](./img/icon/icon-note.gif) **说明：**` thead`和`hisilicon`的代码在内网中，除项目内人员，其他人员无权下载

### 1.4 下载介绍

脚本会在1.2 配置的路径下创建对应的代码版本路径，如果对应的代码版本路径已经存在则会生成一个带`_tmp`后缀的路径，如果该路径仍然存在，则会提示让手动输入路径名字（仅仅是目录的名字而不是完整的路径），如果仍然存在，则程序直接退出，具体的路径在脚本运行下载代码结束后的`log`中有说明，同样脚本所要执行的下载命令`log`中也会显示打印出来，如下所示：

```shell
================================================================================================
you have been download riscv64-qemu OpenHarmony-3.2-Beta2 code
url       : https://isrc.iscas.ac.cn/gitlab/riscv/polyosmobile/ohos_qemu/manifest.git
branch    : OpenHarmony-3.2-Beta2
xml_name  :
code_path : /home/wen_fei/OpenHarmony1/ohos_qemu_riscv64
init   cmd: repo init -u https://isrc.iscas.ac.cn/gitlab/riscv/polyosmobile/ohos_qemu/manifest.git -b OpenHarmony-3.2-Beta2 -m ohos_qemu.xml --no-repo-verify
sync   cmd: repo sync -c
lfs    cmd: repo forall -c 'git lfs pull'
set_br cmd: repo start OpenHarmony-3.2-Beta2 --all
================================================================================================
```

![icon-note.gif](./img/icon/icon-note.gif) **说明:** 重复路径操作方法

```shell
you have already exist following path:
/home/wen_fei/OpenHarmony/thead-3.2-beta2
/home/wen_fei/OpenHarmony/thead-3.2-beta2_tmp
please input the path name you want to download code
if you want to delete the /home/wen_fei/OpenHarmony/thead-3.2-beta2_tmp directly and then download it again, please press Enter.
test  # 直接输入要存放代码的路径名

================================================================================================
you have been download thead-3.2-beta2 OpenHarmony-3.2-Beta2 code
url       : https://10.20.72.61/gitlab.com/ohos/manifest.git
branch    : OpenHarmony-3.2-Beta2
xml_name  :
code_path : /home/wen_fei/OpenHarmony/test
init   cmd: repo init -u https://10.20.72.61/gitlab.com/ohos/manifest.git -b OpenHarmony-3.2-Beta2 -m thead_develop.xml --no-repo-verify
sync   cmd: repo sync -c
lfs    cmd: repo forall -c 'git lfs pull'
set_br cmd: repo start OpenHarmony-3.2-Beta2 --all
================================================================================================

download code success ^_^

```



## 二、脚本使用

```shell
git clone https://gitee.com/personal-summary/code_download.git
cd code_download
./code_download.sh
# 输入所需下载的代码，例如：2.1表示要下载thead-v3.2.2-release的代码
```