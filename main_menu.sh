#!/usr/bin/env bash

char_flag="*"
char_num=44

front_black='30'
front_red='31'
front_green='32'
front_yellow='33'
front_blue='34'
front_purple='35'
front_cyan='36'
front_white='37'

back_black='40'
back_red='41'
back_green='42'
back_yellow='43'
back_blue='44'
back_purple='45'
back_cyan='46'
back_white='47'

font_underline='04'
color_flash='05'

color_prefix='\e['
color_suffix='\e[0m'
color_sep=';'
color_flag='m'

function openharmony_menu() {
  cyan_black=${color_prefix}${front_cyan}${color_sep}${back_black}${color_flag}
  echo -e "* ${cyan_black}OpenHarmony Version${color_suffix}                      *"
  echo -e "*   OpenHarmony master         prese  1.1  *"
  echo -e "*   OpenHarmony-v3.2.2-Release press  1.2  *"
  echo -e "*   OpenHarmony-v4.0-Release   press  1.3  *"
  echo -e "*   OpenHarmony-v4.1-Release   press  1.4  *"
  echo -e "*   OpenHarmony-clang-master   press  1.x  *"
}

function thead_products_menu() {
  yellow_black=${color_prefix}${front_yellow}${color_sep}${back_black}${color_flag}
  echo -e "* ${yellow_black}Thead Products Version${color_suffix}                   *"
  echo -e "*   dayu800-v3.2.2-release     press  2.1  *"
  echo -e "*   dayu800-v4.0-release       press  2.2  *"
  echo -e "*   dayu800-v4.1-release       press  2.3  *"
  echo -e "*   dayu800-5.0-release        press  2.4  *"
  echo -e "*   thead-tablet               press  2.20 *"
  echo -e "*   thead-cartablet            press  2.21 *"
  echo -e "*   thead-cloud                press  2.22 *"
  echo -e "*   dayu800-lt9211-v4.1        press  2.23 *"
  echo -e "*   dayu800-cloud-gateway-v4.1 press  2.24 *"
  echo -e "*   dayu800-v4.1-open-source   press  2.25 *"
  echo -e "*   kernel-xts-master          press  2.26 *"
  echo -e "*   future-city-v3.2.2         press  2.27 *"
  echo -e "*   future-city-v4.1           press  2.28 *"
  echo -e "*   ienvctrl-v4.1              press  2.29 *"
  echo -e "*   duolun-v4.1-release        press  2.30 *"
  echo -e "*   dayu810-v4.0-release       press  2.31 *"
  echo -e "*   dayu800-cloud-zhihe        press  2.32 *"
  echo -e "*   dayu800-zhihe-air          press  2.33 *"
  echo -e "*   dayu800-zhihe-duolun       press  2.34 *"
  echo -e "*   dayu800-thead-pad          press  2.35 *"
  echo -e "*   chromium-v4.0-release      press  2.51 *"
  echo -e "*   chromium-v4.1-release      press  2.52 *"
}

function hisilicon_products_menu() {
  debug=$1
  purple_black=${color_prefix}${front_purple}${color_sep}${back_black}${color_flag}
  echo -e "* ${purple_black}Hisilicon Products Version${color_suffix}               *"
  if [[ ${debug} == "debug" ]]; then
    echo -e "*   dayu900-v4.0-release       press  3.10 *"
  fi
  echo -e "*   dayu900-v4.0-small         press  3.11 *"
  if [[ ${debug} == "debug" ]]; then
    echo -e "*   dayu900-v4.0-small-llvm    press  3.12 *"
    echo -e "*   dayu900-v4.0-emo-emmc      press  3.13 *"
  fi
  echo -e "*   dayu900-v4.0-ebaina-camera press  3.14 *"
  echo -e "*   dayu900-v4.0-sdk           press  3.15 *"
  if [[ ${debug} == "debug" ]]; then
    echo -e "*   dayu900-v4.1-release       press  3.20 *"
  fi
  echo -e "*   ebaina-hi351xdv500         press  3.50 *"
  echo -e "*   ebaina-hi351xdv500-ohos    press  3.51 *"
  echo -e "*   power-v4.0-ebaina          press  3.52 *"
  echo -e "*   power-v4.0-dayu900         press  3.53 *"
  echo -e "*   power-v4.0-dayu900-open    press  3.54 *"
  echo -e "*   power-v4.0-dayu900_sec     press  3.55 *"
  echo -e "*   power-v4.0-release-tag     press  3.x  *"
}

function space_china_menu() {
  debug=$1
  purple_black=${color_prefix}${front_purple}${color_sep}${back_black}${color_flag}
  echo -e "* ${purple_black}Space China Version${color_suffix}                      *"
  echo -e "*   dayu210-v4.1-release       press  4.1  *"
  echo -e "*   speed-v4.1-release         press  4.2  *"
  echo -e "*   hihope-v4.1-release        press  4.3  *"
  echo -e "*   njdg-v4.1-release          press  4.4  *"
  if [[ ${debug} == "debug" ]]; then
    echo -e "*   spacechia-v1.0-release     press  4.50 *"
    echo -e "*   njdg-v1.0-release          press  4.51 *"
  fi
}

function riscv_adapt_menu() {
  blue_black=${color_prefix}${front_blue}${color_sep}${back_black}${color_flag}
  echo -e "* ${blue_black}Riscv Adapt Version${color_suffix}                      *"
  echo -e "*   riscv-weekly0905           press 14.1  *"
  echo -e "*   riscv-3.2-Release          press 14.2  *"
  echo -e "*   riscv-qemu                 press 14.3  *"
}

function local_menu() {
  purple_black=${color_prefix}${front_purple}${color_sep}${back_black}${color_flag}
  echo -e "* ${purple_black}Local Version${color_suffix}                            *"
  echo -e "*   dayu800_v3.2.2_debug       press 15.10 *"
  echo -e "*   bes2600-mine               press 15.11 *"
  echo -e "*   rk_v3.2.2_debug            press 15.20 *"
  echo -e "*   rk_v4.0_debug              press 15.21 *"
  echo -e "*   rk_v4.1_debug              press 15.22 *"
  echo -e "*   android-12.1.0_r4          press 15.30 *"
  echo -e "*   android-13.0.0_r6          press 15.31 *"
  echo -e "*   android-13.0.0_r80         press 15.32 *"
  echo -e "*   android-14.0.0_r10         press 15.33 *"
  echo -e "*   android-10.0.0_r40         press 15.34 *"
  echo -e "*   virt-dev                   press 15.40 *"
  echo -e "*   rk3588-android12           press 15.50 *"
  echo -e "*   all_v4.1                   press 15.60 *"
  echo -e "*   stm32_v4.1_debug           press 15.61 *"
  echo -e "*   bes2600_v4.1_debug         press 15.62 *"
}

function extra_menu() {
  purple_black=${color_prefix}${front_purple}${color_sep}${back_black}${color_flag}
  echo -e "* ${purple_black}Extra Version${color_suffix}                            *"
  echo -e "*   remora_rk3588              press 16.1  *"
  echo -e "*   remora_dayu800             press 16.2  *"
  echo -e "*   thead_dayu800              press 16.3  *"
  echo -e "*   dayu116                    press 16.4  *"
}

function main_menu() {
  debug=$1
  # 打印头
  printf "%-${char_num}s\n" "${char_flag}" | sed "s/ /${char_flag}/g"

  red_flash=${color_prefix}${front_red}${color_sep}${color_flash}${color_flag}
  # 打印欢迎语句
  echo -e "*   ${red_flash}Welcome to download OpenHarmony Code${color_suffix}   *"
  echo -e "* Please Choice OpenHarmony SDK:           *"
  openharmony_menu
  thead_products_menu
  hisilicon_products_menu ${debug}
  space_china_menu ${debug}
  if [[ ${debug} == "debug" ]]; then
    riscv_adapt_menu
    local_menu
    extra_menu
  fi
  # 打印尾
  printf "%-${char_num}s\n" "${char_flag}" | sed "s/ /${char_flag}/g"
}
